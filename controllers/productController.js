const Product = require('../models/product');
const auth = require('../auth');


module.exports.addProduct = (body, file) => {
	let newProduct = new Product({
		albumName: body.albumName,
		artist: body.artist,
		genre: body.genre,
		description: body.description,
		price: body.price,
		quantity: body.quantity,
		isActive: body.isActive,
		image: file.filename
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getSpecificProduct = (params) => {
	return Product.find({_id: params.productId}).then(result => {
		return result;
	})
}

module.exports.updateProduct = (params, body) => {
	let updatedProduct = {
		productName: body.productName,
		description: body.description,
		price: body.price,
		quantity: body.quantity,
		isActive: body.isActive
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.updatePhoto = (params, file) => {
	let updatedPhoto = {
		image: file.filename
	}

	return Product.findByIdAndUpdate(params.productId, updatedPhoto).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.archiveProduct = (params, body) => {
	let archivedProduct;

	if(body.isActive === true){
		archivedProduct = {
			isActive: false
		}
	}else{
		archivedProduct = {
			isActive: true
		}
	}

	return Product.findByIdAndUpdate(params.productId, archivedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}