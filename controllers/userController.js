const User = require('../models/user');
// const Order = require('../models/order')
const auth = require('../auth');
const bcrypt = require('bcrypt');

module.exports.checkEmail = (body) => {
	return User.find({email: body.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName, 
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		mobileNumber: body.mobileNumber,
		address: {
			street1: body.address.street1,
			city: body.address.city,
			province: body.address.province,
			zipCode: body.address.zipCode
		}
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())};
			}else{
				return false;
			}
		}
	})
}

module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined; 
		return result;
	})
}

module.exports.updateAdminStatus = (params, body) => {
	let updatedAdminStatus = {
		isAdmin: body.isAdmin
	}

	return User.findByIdAndUpdate(params.userId, updatedAdminStatus).then((adminStatus, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


module.exports.placeOrders = async (data) => {
	let userSaveStatus = await User.findById({_id: data.userId}).then(user => {user.orders.push({orderId: data.orderId})

		return user.save().then((user, err) => {
			if(err){
				return false
			}else{
				return true
			}
		})
	})

	if(userSaveStatus){
		return true;
	}else{
		return false;
	}
}

