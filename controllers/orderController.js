const Order = require('../models/order');
const User = require('../models/user');
const Product = require('../models/product');
const auth = require('../auth');
const bcrypt = require('bcrypt');


module.exports.createOrder = (body) => {
	let newOrder = new Order({
		userId: body.userId,
		fullName: body.fullName,
		shippingAddress: { 
			country: body.shippingAddress.country, 
			street1: body.shippingAddress.street1,
			city: body.shippingAddress.city, 
			province: body.shippingAddress.province, 
			zipCode: body.shippingAddress.zipCode
		},
		items: body.items,
		totalAmount: body.totalAmount
	})

	return newOrder.save().then((order, error) => {
		if(error){
			return false;
		}else{
			return newOrder;
		}
	})
}

module.exports.addOrderToUser = async (data) => {
	let orderSaveStatus = await User.findById(data.userId).then(user => {user.orders.push({
		orderId: data.orderId,
		items: data.items
	    })

		return user.save().then((user, err) => {
			if(err){
				return false;
			}else{
				return true;
			}
		})
	})

	if(orderSaveStatus){
		return true
	}else{
		return false
	}
}

module.exports.getMyOrders = (params) => {
	return User.findById(params.userId).then(result => {
		return result.orders
	})
}

// module.exports.getOrderInfo = (params) => {
// 	return Order.findById(params.orderId).then(result => {
// 		return result;
// 	})
// }


module.exports.getAllOrders = () => {
	return Order.find({}).then(result => {
		return result;
	})
}