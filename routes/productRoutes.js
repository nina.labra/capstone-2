const express = require('express');
const router = express.Router();
const auth = require('../auth')
const productController = require('../controllers/productController')
const multer = require('multer');
const AWS = require('aws-sdk');

const s3 = new AWS.S3({
	accessKeyId: process.env.AWS_ID
	secretAccessKey: process.env.AWS_SECRET
})

const fileStorageEngine = multer.memoryStorage({
	destination: (req, file, cb) => {
		cb(null, '')
	},
	filename: (req, file, cb) => {
		cb(null, Date.now() + '--' + file.originalname)
	}
})

const upload = multer({storage: fileStorageEngine})

router.post("/", [auth.verify, upload.single('image')], (req, res) => {
	let token = auth.decode(req.headers.authorization)

	console.log(req.file)
	
	if(token.isAdmin === true){
		productController.addProduct(req.body, req.file).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "No authorization for this action"})
	}
})


router.get("/all-active", (req, res) => {
	productController.getAllActive().then(resultFromGetAllActive => res.send(resultFromGetAllActive))
})

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromGetAllProducts => res.send(resultFromGetAllProducts))
})

router.get("/:productId", (req, res) => {
	productController.getSpecificProduct(req.params).then(resultFromGetSpecificProduct => res.send(resultFromGetSpecificProduct))
})

router.put("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	productController.updateProduct(req.params, req.body).then(resultFromUpdateProduct => res.send(resultFromUpdateProduct))
})

router.put("/images/:productId", [auth.verify, upload.single('image')], (req, res) => {
	let token = auth.decode(req.headers.authorization)

	productController.updatePhoto(req.params, req.file).then(resultFromUpdatePhoto => res.send(resultFromUpdatePhoto))
})

router.put("/archive/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
		productController.archiveProduct(req.params, req.body).then(resultFromArchiveProduct => res.send(resultFromArchiveProduct))	
	}else{
		res.send({auth: "No authorization for this action"})
	}
})

module.exports = router;