const express = require('express');
const router = express.Router();
const auth = require('../auth');
const userController = require('../controllers/userController');


router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromCheckEmail => res.send(resultFromCheckEmail))
})


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegisterUser => res.send(resultFromRegisterUser))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

router.get("/profile", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})

router.put("/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
	userController.updateAdminStatus(req.params, req.body).then(resultFromChangeAdmin => res.send(resultFromChangeAdmin))
	}else{
		res.send({auth: "No authorization for this action"})
	}
})

router.post("/orders", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		orderId: req.body.orderId
	}

	userController.placeOrders(data).then(resultFromPlaceOrders => res.send(resultFromPlaceOrders))
})


module.exports = router;