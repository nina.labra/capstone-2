const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/orderController');


router.post("/create-order", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	orderController.createOrder(req.body).then(resultFromCreateCart => res.send(resultFromCreateCart))
})

router.post("/sync-order", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		orderId: req.body.orderId,
		items: req.body.items
	}

	orderController.addOrderToUser(data).then(resultFromEnroll => res.send(resultFromEnroll))
})


router.get("/my-orders/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	orderController.getMyOrders(req.params).then(resultFromGetMyOrders => res.send(resultFromGetMyOrders))
})

// router.get("/:orderId", auth.verify, (req, res) => {
// 	let token = auth.decode(req.headers.authorization)

// 	orderController.getOrderInfo(req.params).then(resultFromGetOrderInfo => res.send(resultFromGetOrderInfo))
// })

router.get("/all-orders", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	// if(token.isAdmin == true){
	orderController.getAllOrders().then(resultFromGetAllOrders => res.send(resultFromGetAllOrders))
	// }else{
	// 	res.send({auth: "Not an admin"})
	// }
})



module.exports = router;