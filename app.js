const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const multer = require('multer')
// const corsOptions = {
// 	origin: ['http://localhost:3001', 'https://competent-wing-b8facc.netlify.app']
// 	optionsSuccessStatus: 200
// }

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();
const port = process.env.PORT || 3001

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/user', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/images', express.static('images'));


mongoose.connect("mongodb+srv://admin:admin123@cluster0.qnql0.mongodb.net/b110_ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
}) 

mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
});
