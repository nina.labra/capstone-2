const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	albumName: {
		type: String, 
		required: [true, "Product name is required"]
	},
	artist: {
		type: String, 
		required: [true, "Artist name is required"]
	},
	genre: {
		type: String, 
		required: [true, "Genre is required"]
	},
	description: {
		type: String, 
		required: [true, "Product description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	quantity: {
		type: Number,
		required: [true, "Product quantity is required"]
	},
	image: {
		data: Buffer,
		type: String
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	isActive: {
		type: Boolean, 
		required: true
	}
})

module.exports = mongoose.model("Product", productSchema)