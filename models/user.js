const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String, 
		required: [true, "Last name is required"]
	},
	email: {
		type: String, 
		required: [true, "Email is required"]
	},
	password: {
		type: String, 
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	address: 
	{	
		street1: {
			type: String, 
			required: [true, "Street address is required"]
		}, 
		city: {
			type: String, 
			required: [true, "City is required"]
		}, 
		province: {
			type: String, 
			required: [true, "Province is required"]
		},
		zipCode:{
			type: Number,
			required: [true, "Zip code is required"] 
		}	
	},
	orders: [{
		orderId: {
			type: String,
		},
		totalAmount: {
			type: Number,
		},
		items: [{
			productId: {
				type: String
			},
			image: {
				type: String
			},
			albumName: {
				type: String
			},
			quantity: {
				type: Number
			},
			price: {
				type: Number
			}
		}],
		createdOn: {
			type: Date,
			default: new Date()
		}
	}]
	
})



module.exports = mongoose.model("User", userSchema);