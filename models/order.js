const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: true
	},
	fullName: {
		type: String,
		required: true
	},
	items: [{
		productId: {
			type: String
		},
		image: {
			type: String
		},
		albumName: {
			type: String
		},
		artist: {
			type: String
		},
		quantity: {
			type: Number
		},
		price: {
			type: Number
		}
	}],
	shippingAddress: {
		street1: {
			type: String, 
			required: [true, "Street address is required"]
		},
		street2: {
			type: String,
			default: "--"
		},
		city: {
			type: String, 
			required: [true, "City is required"]
		}, 
		province: {
			type: String, 
			required: [true, "Province is required"]
		},
		zipCode:{
			type: Number,
			required: [true, "Zip code is required"] 
		}	
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orderStatus: {
		type: String,
		default: "Pending"
	},
	modeOfPayment: {
		type: String
	},
	totalAmount: {
		type: Number,
	}
})	
module.exports = mongoose.model("Order", orderSchema)